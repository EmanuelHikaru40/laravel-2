<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>WEGMAG - @yield('titulo')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css">
</head>

<body>
    <div id="geral">
        <header id="cabecalho">
            <h1>
                <img src="img/logo.png" alt="Web Mag" width="100">
            </h1>
            <nav>
                <ul>
                    <li><a href="{{ route('home') }}" id="home">Home</a></li>
                    <li><a href="{{ route('html') }}">Html</a></li>
                    <li><a href="{{ route('javascripts') }}">Java Scripts</a></li>
                    <li><a href="{{ route('dica-css') }}">Css</a></li>
                    <li><a href="{{ route('videosAulas') }}">Videos Aulas</a></li>
                    <li><a href="{{ route('contato') }}">Contato</a></li>
                </ul>
            </nav>
        </header>

        <section id="conteudo">
            @yield('conteudo')
        </section>
        <div id="rodape">
            <p>Todos os direitos são reservados</p>
        </div>
    </div>
</body>

</html>
